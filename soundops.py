from mysound import Sound


def soundadd(S1: Sound, S2: Sound) -> Sound:
    sum_sound = Sound(max(S1.duration, S2.duration))
    for i in range(len(sum_sound.buffer)):
        if i < len(S1.buffer) and i < len(S2.buffer):
            sum_sound.buffer[i] = S1.buffer[i] + S2.buffer[i]
        elif i < len(S1.buffer):
            sum_sound.buffer[i] = S1.buffer[i]
        elif i < len(S2.buffer):
            sum_sound.buffer[i] = S2.buffer[i]
    return sum_sound


s1 = Sound(1)
s1.sin(22, 50000)
s2 = Sound(2)
s2.sin(22, 50000)
print(soundadd(s1, s2).bars(0.1))

# Sin errores detectados por pycodestyle
