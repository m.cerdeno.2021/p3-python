# p3-python

Repositorio de plantilla para la práctica 3 (Python avanzado) de PTAVI.

Recuerda que antes de empezar a trabajar con esta practica deberías realizar un
fork de este repositorio, con lo que crearás un nuevo repositorio en tu cuenta,
cuyos contenidos serán una copia de los de este.
Luego tendrás que clonar ese repositorio para tener una copia local con
la que podrás trabajar. No olvides luego hacer commits frecuentes, y
sincronizarlos con el servidor (`git push`) antes de la fecha de entrega
final de la práctica. Recuerda también que para que pueda ser corregido, el
repositorio deberá ser público a partir de la fecha de entrega.

[Enunciado de esta práctica](https://gitlab.com/cursomminet/code/-/blob/master/p3-python/ejercicios.md).

Como le comenté en el correo, quería explicarle una confusión que me ha surgido a la hora de adjuntarle en el repositorio de Gitlab la práctica, ya que en el ejercicio 9, habla de subir los ficheros modificados sin errores detectados en pycodestyle, por lo que he subido todos los ficheros ya sin errores en el mismo commit llamado "Ejercicio 9"; aunque cada ejercicio estaba anteriormente subido con un commit con su respectivo nombre de ejercicio, me causaba dudas ver en el repositorio todos como ejercicio 9 y que pudiera llevar a confusión esa nomenclatura. Por otra parte, algunos ejercicios tienen más de un commit, ya que he ido corrigiendo los errores.
