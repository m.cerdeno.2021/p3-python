import unittest
from mysoundsin import Sound
from soundops import soundadd


class TestSoundadd(unittest.TestCase):

    def test_distintas_duraciones(self):
        s1 = Sound(1)
        s1.sin(440, 44100)

        s2 = Sound(2)
        s2.sin(660, 88200)

        result = soundadd(s1, s2)

        self.assertEqual(result.duration, 2)
        self.assertEqual(len(result.buffer), 88200)

    def test_un_sonido(self):
        s1 = Sound(1)
        s1.sin(440, 44100)

        s2 = Sound(2)

        result = soundadd(s1, s2)

        self.assertEqual(result.duration, 2)
        self.assertEqual(len(result.buffer), 88200)

    def test_bars(self):
        s1 = Sound(3)
        s2 = Sound(2)

        s1.sin(440, 10000)
        s2.sin(440, 10000)
        result = soundadd(s1, s2).bars(0.0001)

        # Check number of lines
        self.assertAlmostEqual(33076, len(result.split('\n')))

# Sin errores detectados por pycodestyle
