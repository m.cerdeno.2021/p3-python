import unittest
from mysoundsin import SoundSin


class test_SoundSin(unittest.TestCase):

    def test_seno(self):
        seno = SoundSin(1, 440, 10000)
        self.assertEqual(seno.buffer[0], 0)

    def test_seno2(self):
        seno = SoundSin(2, 55555, 1000)
        self.assertEqual(seno.duration, 2)
        self.assertEqual(seno.frequency, 55555)
        self.assertEqual(seno.amplitude, 1000)

    def test_sound(self):
        seno = SoundSin(1, 440, 10000)
        bars = seno.bars(0.001)
        self.assertAlmostEqual(len(bars.split('\n')), 1004)

# Sin errores detectados por pycodestyle
