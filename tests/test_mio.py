import unittest
from mysound import Sound


class TestSound(unittest.TestCase):

    def test_seno(self):
        sound = Sound(10)
        self.assertTrue(44100 * 10, len(sound.buffer))

    def test_bar(self):
        sound = Sound(2)
        self.assertTrue(sound.nsamples, 88200)

    def test_seno2(self):
        sound = Sound(2)
        sound.sin(440, 1)
        self.assertEqual(sound.buffer[0], 0)

# Sin errores detectados por pycodestyle
